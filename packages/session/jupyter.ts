<TeXmacs|2.1.2>

<style|source>

<\body>
  <\active*>
    <\src-title>
      <src-package|jupyter|1.0>

      <src-purpose|Markup for Jupyter sessions.>
    </src-title>
  </active*>

  <\active*>
    <\src-comment>
      Set the programming language (for highlighting).
    </src-comment>
  </active*>

  <assign|jupyter-input|<\macro|prompt|body>
    <with|prog-language|<extern|jupyter-session-to-lang|<value|prog-session>>|<generic-input|<arg|prompt>|<arg|body>>>
  </macro>>
</body>

<initial|<\collection>
</collection>>