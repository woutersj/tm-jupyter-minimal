;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; MODULE      : init-jupyter.scm
;; DESCRIPTION : Initialize Jupyter plugin
;; COPYRIGHT   : (C) 2019  Massimiliano Gubinelli
;; COPYRIGHT   : (C) 2021  Jeroen Wouters
;;
;; This software falls under the GNU general public license version 3 or later.
;; It comes WITHOUT ANY WARRANTY WHATSOEVER. For details, see the file LICENSE
;; in the root directory or <http://www.gnu.org/licenses/gpl-3.0.html>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(if (defined? 'xmacs-version)
  (begin
    (use-modules (binary python3))
    (define (python-command) (url->string (find-binary-python3)))))

(define (jupyter-serialize lan t)
    (with u (pre-serialize lan t)
      (with s (texmacs->code (stree->tree u) "SourceCode")
        (string-append  s  "\n<EOF>\n"))))

;; There should really be a mechanism for plugins to customise the way the
;; `prog-lang` environment variable is derived from the plugin and session name
(tm-define (jupyter-session-to-lang session)
  (:secure #t)
  (let ((s (tree->string session)))
    (cond
      ((string-starts? s "python") "python")
      ((string-starts? s "julia") "julia")
      ((string-starts? s "ir") "r")
      ((string=? s "default") "python")
      (else s))))

(define (jupyter-launcher kernel)
    (let ((command 
           (string-append (python-command) " "
            (system-url->string "$TEXMACS_HOME_PATH/plugins/jupyter/tm_jupyter/client.py")
            )))
    (if (!= kernel "") (string-append command " --kernel=" kernel)
      command)))

(define (jupyter-launchers)
    (map (lambda (u) `(:launch ,u ,(jupyter-launcher u)))
      (filter (lambda (k) (!= "" k))
        (let ((command
               (string-append (python-command) " "
            (system-url->string "$TEXMACS_HOME_PATH/plugins/jupyter/tm_jupyter/kernels.py"))))
        (string-split 
         (string-replace (eval-system command) "\r" "")
         #\newline))
    )))

(plugin-configure jupyter
  (:require (url-exists-in-path? "python"))
  (:launch ,(jupyter-launcher ""))
  ,@(jupyter-launchers)
  (:serializer ,jupyter-serialize)
  (:session "Jupyter")
  (:tab-completion #t))

(when (supports-jupyter?)
  (import-from (jupyter-kernels))
  (import-from (jupyter-format)))
